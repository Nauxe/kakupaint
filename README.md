# README #

Kaku Paint! is a paint program in a console window. Yes, there's mspaint and yes; I did it just because I can.

Some prelimenary stuff that you need to do first in order to use Kaku Paint!.

### Set yo console buffers right ###

* Go to your console settings window and set your **screen buffer width** to at least 121 characters.
* The colours should be stock or close to stock.
* The console font should be as close to 1:1 aspect ratio.