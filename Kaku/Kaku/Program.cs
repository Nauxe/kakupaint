﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Kaku
{
    class Program
    {
        public static List<List<ConsoleColor>> ImageMap = new List<List<ConsoleColor>> { };
        public static List<int> reverseMap = new List<int> { 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
        public static int curX = 1;
        public static int curY = 1;
        public static int NewCurX = 1;
        public static int NewCurY = 1;
        public static int ink = 15;
        public static ConsoleColor[] cols = (ConsoleColor[])ConsoleColor.GetValues(typeof(ConsoleColor)); 

        //Set Canvas Size Here!
        public static int CanvasSizeX = 120; //Width
        public static int CanvasSizeY = 85; //Height

        static void Main(string[] args)
        {
            Console.WriteLine("Initalizeing Canvas..");
            Console.WriteLine("Done!");
            Serializer serial_ = new Serializer();
            ImageMap = serial_.DeSerializeObject<List<List<ConsoleColor>>>("kaku.kpf");
            refreshCanvas();
            writeTitle();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();
            initalizeList(CanvasSizeX, CanvasSizeY);
            //Do some firsttiem ints here
            ConsoleKeyInfo key;
            showMenu();
            drawBorder();
            //Ends
            while (true) {
                key = Console.ReadKey(true);
                //Console.WriteLine("[CHAR] [{0}]", key.Key);
                if (key.Key.ToString() == "DownArrow") {
                    if (key.Modifiers == ConsoleModifiers.Shift)
                    {
                        ImageMap[curX][curY] = cols[ink];
                    }
                    NewCurY++;
                    moveCursor();
                }

                if (key.Key.ToString() == "UpArrow")
                {
                    if (key.Modifiers == ConsoleModifiers.Shift)
                    {
                        ImageMap[curX][curY] = cols[ink];
                    }
                    NewCurY--;
                    moveCursor();
                }

                if (key.Key.ToString() == "LeftArrow")
                {
                    if (key.Modifiers == ConsoleModifiers.Shift)
                    {
                        ImageMap[curX][curY] = cols[ink];
                    }
                    NewCurX--;
                    moveCursor();
                }

                if (key.Key.ToString() == "RightArrow")
                {
                    if (key.Modifiers == ConsoleModifiers.Shift) {
                        ImageMap[curX][curY] = cols[ink];
                    }
                    NewCurX++;
                    moveCursor();
                }

                if (key.Key.ToString() == "Spacebar")
                {
                    ImageMap[curX][curY] = cols[ink];
                    moveCursor();
                }

                if (key.Key.ToString() == "Q")
                {
                    if (ink > 0) {
                        ink--;
                    }
                    writeTitle();
                    moveCursor();
                }

                if (key.Key.ToString() == "W")
                {
                    if (ink < cols.Count() - 1)
                    {
                        ink++;
                    }
                    writeTitle();
                    moveCursor();
                }

                if (key.Key.ToString() == "S")
                {
                    showMenu();
                    Console.Write("[SAVE] Enter Filename: "); string filename = Console.ReadLine();
                    Serializer serial = new Serializer();
                    serial.SerializeObject(ImageMap, filename);
                    Console.Clear();
                    refreshCanvas();
                    drawBorder();
                    showMenu();
                }

                if (key.Key.ToString() == "D")
                {
                    showMenu();
                    Console.Write("[OPEN] Enter Filename: "); string filename = Console.ReadLine();
                    Serializer serial = new Serializer();
                    ImageMap = serial.DeSerializeObject<List<List<ConsoleColor>>>(filename);
                    CanvasSizeX = ImageMap.Count;
                    CanvasSizeY = ImageMap[0].Count;
                    Console.Clear();
                    refreshCanvas();
                    drawBorder();
                    showMenu();
                }

            }

        }

        static void initalizeList(int x, int y) {
            ImageMap.Clear();
            for (int i = 0; i < x; i++) {
                
                ImageMap.Add(new List<ConsoleColor> { });
                for (int u = 0; u < y; u++) {
                    ImageMap[i].Add(ConsoleColor.Black);
                }
            }
        }

        static void moveCursor() {
            try
            {
                if ((NewCurX < CanvasSizeX && NewCurY < CanvasSizeY) && (NewCurX > 0 && NewCurY > 0))
                {
                    Console.BackgroundColor = ImageMap[curX][curY];
                    //Console.ForegroundColor = cols[reverseMap[ink]];
                    Console.SetCursorPosition(curX, curY);
                    Console.Write(" ");
                    Console.SetCursorPosition(NewCurX, NewCurY);
                    Console.BackgroundColor = cols[ink];
                    Console.ForegroundColor = cols[reverseMap[ink]];
                    Console.Write("∙");
                    Console.SetCursorPosition(NewCurX, NewCurY);
                    curX = NewCurX;
                    curY = NewCurY;
                }
                else {
                    NewCurX = curX;
                    NewCurY = curY;
                }
            }
            catch {
                NewCurX = curX;
                NewCurY = curY;
            }
            writeTitle();
        }

        static void showMenu() {
            Console.CursorVisible = false;
            Console.SetCursorPosition(0, CanvasSizeY + 1);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;   
            Console.Write("Kaku Paint -- [ARROWS]:Move Cursors  [Q-W]:Change Ink  [Spacebar]:Paint   [Shift]:Contionous Paint\n");
            Console.Write("[S]:Save Image   [D]:Load File");
            Console.ForegroundColor = cols[ink];
            Console.BackgroundColor = ConsoleColor.Black; 
        }

        static void writeTitle() {
            Console.Title = "Kaku Paint! "+CanvasSizeX +"X"+ CanvasSizeY+" :: X " + curX + " Y " + curY + " :: IM " + ImageMap[curX][curY].ToString() + " :: INK " + cols[ink].ToString();

        }

        static void drawBorder() {
            bool next = true;
            
            Console.SetCursorPosition(0,0);
            for (int i = 0; i < CanvasSizeX; i++) {
                if (next)
                {
                    Console.BackgroundColor = ConsoleColor.DarkBlue;

                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.White;
                }
                next = !next;
                Console.Write(" ");
                Thread.Sleep(1);
            }
            Console.SetCursorPosition(CanvasSizeX - 1, 0);
            for (int i = 0; i < CanvasSizeY; i++)
            {
                Console.SetCursorPosition(CanvasSizeX, i);
                if (next)
                {
                    Console.BackgroundColor = ConsoleColor.DarkBlue;

                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.White;
                }
                next = !next;
                Console.Write(" ");
                Thread.Sleep(1);
            }
            for (int i = 1; i < CanvasSizeX; i++)
            {
                Console.SetCursorPosition(i, CanvasSizeY );
                if (next)
                {
                    Console.BackgroundColor = ConsoleColor.DarkBlue;

                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.White;
                }
                next = !next;
                Console.Write(" ");
                Thread.Sleep(1);
            }
            for (int i = 0; i < CanvasSizeY; i++)
            {
                Console.SetCursorPosition(0, i);
                if (next)
                {
                    Console.BackgroundColor = ConsoleColor.DarkBlue;

                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.White;
                }
                next = !next;
                Console.Write(" ");
                Thread.Sleep(1);
            }
            //for (int i = 0; i < CanvasSizeX; i++)
            //{
            //    Console.SetCursorPosition(i, CanvasSizeY - 1);
            //    Console.BackgroundColor = ConsoleColor.White;
            //    Console.Write(" ");
            //}
        }
        static void refreshCanvas() {
            for (var x = 0; x < CanvasSizeX; x++) {
                Console.Title = string.Format("Reloading Canvas..");
                for (var y = 0; y < CanvasSizeY; y++) {
                    
                    if (ImageMap[x][y] != ConsoleColor.Black) {
                        NewCurX = x;
                        NewCurY = y;
                        moveCursor();                   
                    }

                }
                Console.Title = string.Format("[DONE!] Kaku Paint!");
                                
            }
        
        }
    }


    public class Serializer
    {
        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public void SerializeObject<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null) { return; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(fileName);
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }
        }


        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                string attributeXml = string.Empty;

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }

            return objectOut;
        }



    }

}
